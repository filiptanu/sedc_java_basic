package mk.sedc.javabasic.bankaccount;

public class Info {

    public static void printMenu() {
        System.out.println("1. Deposit money");
        System.out.println("2. Withdraw money");
        System.out.println("3. Check balance");
        System.out.println("0. Quit");
        System.out.println("Your choice: ");
    }

    public static void printDeposit() {
        System.out.println("Amount to deposit: ");
    }

    public static void printWithdraw() {
        System.out.println("Amount to withdraw: ");
    }

    public static void printWrongChoice() {
        System.out.println("Wrong choice.");
    }

    public static void printBye() {
        System.out.println("Bye.");
    }

}