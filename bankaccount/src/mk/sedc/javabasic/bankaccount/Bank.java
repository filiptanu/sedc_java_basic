package mk.sedc.javabasic.bankaccount;

import java.util.Scanner;

public class Bank {

    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount();

        Scanner input = new Scanner(System.in);
        int userChoice;
        boolean quit = false;

        do {
            Info.printMenu();

            userChoice = input.nextInt();

            float amount;

            switch (userChoice) {
                case 1:
                    Info.printDeposit();
                    amount = input.nextFloat();
                    bankAccount.deposit(amount);

                    break;
                case 2:
                    Info.printWithdraw();
                    amount = input.nextFloat();
                    bankAccount.withdraw(amount);

                    break;
                case 3:
                    bankAccount.checkBalance();

                    break;
                case 0:
                    quit = true;

                    break;
                default:
                    Info.printWrongChoice();
                    break;
            }
        } while (!quit);

        Info.printBye();
    }

}
