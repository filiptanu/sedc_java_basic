package mk.sedc.javabasic.bankaccount;

public class BankAccount {

    private float balance;

    public BankAccount() {
        balance = 0.0f;
    }

    public void deposit(float amount) {
        if (amount <= 0)
            System.out.println("You cannot deposit a negative amount.");
        else {
            balance += amount;
            System.out.println("You deposited $" + amount + ".");
        }
    }

    public void withdraw(float amount) {
        if (amount <= 0 || amount > balance)
            System.out.println("Withdrawal cannot be completed.");
        else {
            balance -= amount;
            System.out.println("You have withdrawn $" + amount + ".");
        }
    }

    public void checkBalance() {
        System.out.println("Balance: $" + balance);
    }

}
