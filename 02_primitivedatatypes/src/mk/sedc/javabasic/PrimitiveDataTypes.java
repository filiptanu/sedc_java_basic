package mk.sedc.javabasic;

public class PrimitiveDataTypes {

    public static void main(String[] args) {
        // Example #1

        // Integer data types
        int year = 2017;
        // Use the + operator to concatenate the text and the number
        System.out.println("This is the year " + year);

        int anotherYear = year;
        System.out.println("Now you can see what's inside the variable anotherYear - " + anotherYear);

        // You cannot use a keyword for a variable's name
//        int class = 5;

        // Note the L at the end, indicating this is a long value
        long longNumber = 3000000000L;
        System.out.println("This is a really big number: " + longNumber);

        // You have to assign a value to a variable before you use it
//        int unassigned;
//        System.out.println(unassigned);

        // char is an integer type after all, only it is displayed using the UTF-16 encoding scheme
        char firstLetter = 'A';
        System.out.println("The first letter of the alphabet is: " + firstLetter);
        // The following thing is valid
        char lastLetter = 90;
        System.out.println("The last letter of the alphabet is: " + lastLetter);
        // an international encoding standard by which each letter, digit, or symbol is assigned a unique numeric value
        // Java maps the value of the char variable to the appropriate character representation in Unicode
        // Unicode is a superset of ASCII, and the numbers 0–128 have the same meaning in ASCII as they have in Unicode
        // see: https://unicode-table.com/
        // see: http://www.asciitable.com/

        byte smallStuff = 17;
        System.out.println("byte can contain 8 bits: " + smallStuff);

        // You cannot assign a value to a variable that is out of it's range
//        byte overflow = 128;

        // You cannot assign a floating point number to an integer number
//        int integerValue = 1.23;

        int i = 2147483647; // This is the maximum value that an int variable can contain
        System.out.println("i before adding 1: " + i);
        i = i + 1; // If you increment an int beyond it's max value, it will wrap to the min value an int variable can hold
        System.out.println("i after adding 1: " + i);


        // Floating point data types
        double pi = 3.1415; // By default, Java always considers this as a double
        System.out.println("The value of pi is: " + pi);

        // Note the f at the end, indicating this is a float value
        float price = 39.95F;
        System.out.println("The price is: " + price);


        // Boolean data type
        boolean isJavaAwesome = true;
        System.out.println("Is java awesome? " + isJavaAwesome);

        // Since Java 7, any number of underscore characters (_) can appear anywhere between digits in a numeric literal, to improve readability.
        long creditCard = 1234_5678_9012_3456L;
        float pi2 = 3.14_15F;
        // You cannot put _ at the beginning or the end of a number, adjacent to a decimal point, or prior to F or L suffix.
        // int number = 123_;
        // double decimalNumber = 1._23;
//         float anotherDecimalNumber = 1.23_F;

        // End of Example #1


        // Example #2

        // Data conversion (casting) can happen between two data types
        int a = 123;
        long b = a; // casting is not needed
        System.out.println("a: " + a);
        System.out.println("b: " + b);

        short c = 456;
        byte d = (byte) c; // explicit casting is needed
        System.out.println("c: " + c);
        System.out.println("d: " + d); // Note that this may not be the same number as you'd expect

        float e = 1.23f;
        int f = (int) e; // loss of data
        System.out.println("e: " + e);
        System.out.println("f: " + f);

        // End of Example #2
    }

}