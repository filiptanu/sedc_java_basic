package mk.sedc.javabasic.polymorphism;

// Example #2
public class PolymorphismTest {

    public static void main(String[] args) {
        // Any Java object that can pass more than one IS-A test is considered to be polymorphic
        // All of these references point to the same object on the heap
        Deer d = new Deer();
        Animal a = d;
        Vegetarian v = d;
        Object o = d;
    }

}
