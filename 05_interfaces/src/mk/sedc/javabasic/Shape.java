package mk.sedc.javabasic;

public interface Shape {

    // Since Java 8 it is possible to define a default implementation of a method in an interface
    // This change was added because now it is possible to add methods to an interface without
    // breaking existing clients
    default void draw() {
        System.out.println("Drawing a shape");
    }
    double getArea();
    // Example #4
    default void printShape() {
        System.out.println("Default method for printing shape...");
    }

}
