package mk.sedc.javabasic;

// Example #1
public class InterfacesTest {

    public static void main(String[] args) {
        Circle circle1 = new Circle(5.0);
        circle1.draw();
        System.out.println(circle1.getArea());

        Rectangle rectangle1 = new Rectangle(1.5, 2.7);
        rectangle1.draw();
        System.out.println(rectangle1.getArea());


        Shape circle2 = new Circle(7.5);
        Shape rectangle2 = new Rectangle(5.3, 2.7);
    }

}
