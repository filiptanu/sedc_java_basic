package mk.sedc.javabasic.selfdrivingcar;

// Toyota's implementation will be quite different from BMW's
// but both will adhere to the same interface
public class OperateToyota implements OperateCar {

  @Override
  public void turn(int direction, double radius, double startSpeed, double endSpeed) {
      System.out.println("Toyota's implementation of turn()");
  }

  @Override
  public void changeLanes(int direction, double startSpeed, double endSpeed) {
      System.out.println("Toyota's implementation of changeLanes()");
  }

  @Override
  public void signalTurn(int direction, boolean signalOn) {
      System.out.println("Toyota's implementation of turn.");
  }

}