package mk.sedc.javabasic.selfdrivingcar;

public interface OperateCar {

    // constant declarations, if any

    void turn(int direction, double radius, double startSpeed, double endSpeed);
    void changeLanes(int direction, double startSpeed, double endSpeed);
    void signalTurn(int direction, boolean signalOn);

    // more method signatures

}
