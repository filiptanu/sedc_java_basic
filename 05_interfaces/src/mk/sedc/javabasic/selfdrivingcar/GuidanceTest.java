package mk.sedc.javabasic.selfdrivingcar;

// Example #5
public class GuidanceTest {

    public static void main(String[] args) {
        // im BMW cars
        OperateCar bmw = new OperateBMW();
        GuidanceClient guideBmw = new GuidanceClient(bmw);
        guideBmw.receiveGpsData(23.45, 87.65);

        // in Toyota cars
        OperateCar toyota = new OperateToyota();
        GuidanceClient guideToyota = new GuidanceClient(toyota);
        guideToyota.receiveGpsData(12.34, 56.78);
    }

}
