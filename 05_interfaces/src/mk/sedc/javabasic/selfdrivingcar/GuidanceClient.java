package mk.sedc.javabasic.selfdrivingcar;

public class GuidanceClient {

    private OperateCar operateCar;

    public GuidanceClient(OperateCar operateCar) {
        this.operateCar = operateCar;
    }

    public void receiveGpsData(double latitue, double longitude) {
        // operate the car based on the received info
        operateCar.signalTurn(-1, true);
        operateCar.turn(0, 45.0, 1.2, 0.8);
        operateCar.signalTurn(-1, false);

        operateCar.signalTurn(1, true);
        operateCar.changeLanes(-1, 40.0, 45.0);
        operateCar.signalTurn(1, false);
    }

}
