package mk.sedc.javabasic.selfdrivingcar;

public class OperateBMW implements OperateCar {

    @Override
    public void turn(int direction, double radius, double startSpeed, double endSpeed) {
        System.out.println("BMW's implementation of turn()");
    }

    @Override
    public void changeLanes(int direction, double startSpeed, double endSpeed) {
        System.out.println("BMW's implementation of changeLanes()");
    }

    @Override
    public void signalTurn(int direction, boolean signalOn) {
        System.out.println("BMW's implementation of turn.");
    }

}
