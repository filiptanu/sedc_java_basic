package mk.sedc.javabasic.multipleinheritance;

// Example #3
public class MultipleInheritanceTest {

    public static void main(String[] args) {
        ABImplementation ab = new ABImplementation();
        ab.methodA();
        ab.methodB();
    }

}