package mk.sedc.javabasic.multipleinheritance;

public interface InterfaceC extends InterfaceD {

    void methodC();

}
