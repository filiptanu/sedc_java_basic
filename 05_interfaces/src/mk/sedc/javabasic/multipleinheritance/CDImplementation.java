package mk.sedc.javabasic.multipleinheritance;

public class CDImplementation implements InterfaceC {

    @Override
    public void methodC() {
        System.out.println("Implementing C...");
    }

    @Override
    public void methodD() {
        System.out.println("Implementing D...");
    }

}
