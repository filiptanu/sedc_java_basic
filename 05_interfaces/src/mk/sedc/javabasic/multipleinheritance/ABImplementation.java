package mk.sedc.javabasic.multipleinheritance;

public class ABImplementation implements InterfaceA, InterfaceB{

    @Override
    public void methodA() {
        System.out.println("Implementing A...");
    }

    @Override
    public void methodB() {
        System.out.println("Implementing B...");
    }

}
