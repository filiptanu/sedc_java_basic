package mk.sedc.javabasic;

public class Maximum {

    public static void main(String[] args) {
        int[] numbers = {4, 7, 19, 3, 23, 65, 43, 91};

        int max = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max)
                max = numbers[i];
        }

        System.out.println("The maximum number is " + max);
    }

}
