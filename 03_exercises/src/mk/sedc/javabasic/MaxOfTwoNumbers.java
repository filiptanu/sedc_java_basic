package mk.sedc.javabasic;

public class MaxOfTwoNumbers {

    public static void main(String[] args) {
        int a = 7;
        int b = 4;

        if (a > b)
            System.out.println("The maximum is " + a);
        else
            System.out.println("The maximum is " + b);
    }

}
