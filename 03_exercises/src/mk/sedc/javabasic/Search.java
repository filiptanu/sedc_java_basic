package mk.sedc.javabasic;

public class Search {

    public static void main(String[] args) {
        int[] numbers = {4, 7, 19, 3, 23, 65, 43, 91};
        int key = 5;

        int foundIndex = -1;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == key) {
                foundIndex = i;
                break;
            }
        }

        System.out.println(foundIndex);
    }

}
