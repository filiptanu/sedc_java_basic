package mk.sedc.javabasic;

public class Average {

    public static void main(String[] args) {
        int[] numbers = {4, 7, 19, 3, 23, 65, 43, 91};

        int sum = 0;

        for (int i = 0; i < numbers.length; i++)
            sum += numbers[i];

        double avg = sum / numbers.length;

        System.out.format("The average is %.2f", avg);
    }

}
