package mk.sedc.javabasic;

public class Factorial {

    public static void main(String[] args) {
        int num = 6;
        int factorial = 1;

        System.out.println("Number " + num);

        while (num > 0) {
            factorial *= num;
            num--;
        }

        System.out.println("Factorial " + factorial);
    }

}
