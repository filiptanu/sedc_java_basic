package mk.sedc.javabasic;

public class FirstThree {

    public static void main(String[] args) {
        int[] numbers = {4, 7, 19, 3, 23, 65, 43, 91};

        for (int i = 0; i < numbers.length; i++) {
            if (i == 3)
                break;

            System.out.println(numbers[i]);
        }

        // Alternative solution
        for (int i = 0; i < numbers.length && i < 3; i++) {
            System.out.println(numbers[i]);
        }
    }

}
