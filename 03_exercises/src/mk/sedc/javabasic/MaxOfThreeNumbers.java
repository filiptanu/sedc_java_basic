package mk.sedc.javabasic;

public class MaxOfThreeNumbers {

    public static void main(String[] args) {
        int a = 4;
        int b = 7;
        int c = 1;

        int max = 0;

        if (a >= b && a >= c)
            max = a;
        if (b >= a && b >= c)
            max = b;
        if (c >= a && c >= b)
            max = c;

        System.out.println("The maximum is " + max);
    }

}
