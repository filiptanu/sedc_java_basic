package mk.sedc.javabasic;

public class Duplicates {

    public static void main(String[] args) {
        int[] numbers = {4, 7, 19, 3, 23, 7, 65, 43, 7, 91};
        int number = 7;

        int count = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == number)
                count++;
        }

        System.out.println(count);
    }

}
