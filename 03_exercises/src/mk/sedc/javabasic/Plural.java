package mk.sedc.javabasic;

public class Plural {

    public static void main(String[] args) {
        int apples = 1;

        System.out.print("I have " + apples + " apple");
        if (apples > 1)
            System.out.print("s");
    }

}
