package mk.sedc.javabasic;

public class OddNumbers {

    public static void main(String[] args) {
        // Using while
        int n = 0;

        while (n < 20) {
            if (n % 2 == 1)
                System.out.println(n);
            n++;
        }

        // Using for
        for (int i = 0; i < 20; i++) {
            if (i % 2 == 1)
                System.out.println(i);
        }

        // Using for with contine
        for (int i = 0; i < 20; i++) {
            if (i % 2 != 1)
                continue;

            System.out.println(i);
        }
    }

}
