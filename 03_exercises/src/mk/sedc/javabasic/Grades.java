package mk.sedc.javabasic;

public class Grades {

    public static void main(String[] args) {
        int testScore = 75;

        int grade;

        if (testScore >= 90)
            grade = 10;
        else if (testScore >= 80)
            grade = 9;
        else if (testScore >= 70)
            grade = 8;
        else if (testScore >= 60)
            grade = 7;
        else if (testScore >= 50)
            grade = 6;
        else
            grade = 5;

        System.out.println("Your grade for the test is " + grade);
    }

}
