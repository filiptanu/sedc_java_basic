package mk.sedc.javabasic;

public class Operators {

    public static void main(String[] args) {
        // Arithmetic operators
        int a = 23;
        int b = 10;

        int addition = a + b;
        System.out.println("a + b = " + addition);

        int subtraction = a - b;
        System.out.println("a - b = " + subtraction);

        int multiplication = a * b;
        System.out.println("a * b = " + multiplication);

        // Note that this is integer division
        int integerDivision = a / b;
        System.out.println("a / b = " + integerDivision);

        int modulo = a % b;
        System.out.println("a % b = " + modulo);

        // Increment and decrement operators (in their two forms - pre and post)
        // The post increment operator first prints the value of a, and then increments it
        System.out.println("postincrement: " + a++); // The same as a = a + 1;
        // The pre increment operator first decrements the value of b, and then prints it
        System.out.println("preincrement: " + --b); // The same as b = b - 1;

        // Unary + and -
        int positive = +4;
        System.out.println("positive:" + positive);
        int negative = -2;
        System.out.println("negative:" + negative);

        float c = 1.23f;
        float d = 0.21f;
        float floatDivision = c / d;
        System.out.println("c / d = " + floatDivision);

        // Numeric promotion
        int i = 137;
        float f = 0.25f;
        float result = i + f;	// The i is promoted to float and the result is float

        // You can use cast to do a floating point division
        // Convert one of the variables to a float/double and the result will be float/double, since numeric promotion will happen
        float floatDivision2 = (float) a / b;
//        float floatDivision2 = a / (float) b;

        // byte, short and char always get promoted to int when doing arithmetic operations
//        byte b1 = 12;
//        int i2 = 13;
//        byte byteResult = b1 + i2; // Not allowed, since this is an int operation, the result is int


        // Relational operators

        int num1 = 15;
        int num2 = 23;

        System.out.println("num1 == num2: " + (num1 == num2)); // equal
        System.out.println("num1 != num2: " + (num1 != num2)); // not equal
        System.out.println("num1 > num2: " + (num1 > num2)); // greater than
        System.out.println("num1 < num2: " + (num1 < num2)); // less than
        System.out.println("num1 >= num2: " + (num1 >= num2)); // greater than or equal to
        System.out.println("num1 <= num2: " + (num1 <= num2)); // less than or equal to


        // Logical operators

        boolean p = true;
        boolean q = false;

        System.out.println("!p: " + !p); // logical not
        System.out.println("p && q: " + (p && q)); // logical and
        System.out.println("p || q: " + (p || q)); // logical or


        // Assignment operators

        int num3 = 12;
        System.out.println("num3: " + num3);

        num3 += 3;
        System.out.println("num3 += 3: " + num3);

        num3 -= 1;
        System.out.println("num3 -= 1: " + num3);

        num3 *= 4;
        System.out.println("num3 *= 4: " + num3);

        num3 /= 2;
        System.out.println("num3 /= 2: " + num3);

        num3 %= 13;
        System.out.println("num3 %= 13: " + num3);


        // Operator precedence

        int value1 = 5 + 3 * 2; // 11
        System.out.println("value1 = " + value1);

        int value2 = (3 + 2) * 3; // 15
        System.out.println("value2 = " + value2);

        int value3 = 1 + value2++ - 3 + 4 * ++value1; // 61
        System.out.println("value3 = " + value3);
        System.out.println("value2 after increment = " + value2);

        System.out.println(15 + 3 > 19);
    }

}
