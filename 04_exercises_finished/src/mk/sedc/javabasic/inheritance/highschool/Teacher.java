package mk.sedc.javabasic.inheritance.highschool;

public class Teacher extends Person {

    private String subject;
    private int salary;

    public Teacher(String name, int age, String gender, String subject, int salary) {
        super(name, age, gender);
        this.subject = subject;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() + ", subject: " + subject + ", salary: " + salary;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

}
