package mk.sedc.javabasic.guessthenumber;

import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber {

    public static void main(String[] args) {
        Random random = new Random();

        int numberToGuess = 1 + random.nextInt(10);
        int numberOfTries = 0;

        Scanner input = new Scanner(System.in);

        int guess;
        boolean win = false;

        while (!win) {
            System.out.println("Guess a number between 1 and 10: ");
            guess = input.nextInt();
            numberOfTries++;

            if (guess == numberToGuess)
                win = true;
            else if (guess < numberToGuess)
                System.out.println("Your guess is too low");
            else
                System.out.println("Your guess is too high");
        }

        System.out.println("You win!");
        System.out.println("The number was " + numberToGuess);
        System.out.println("It took you " + numberOfTries + " tries");
    }

}