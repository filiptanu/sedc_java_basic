package mk.sedc.javabasic;

public class Arrays {

    public static void main(String[] args) {
        // Declare an array
        int[] daysInMonth;
//        int daysInMonth[];    // This works as well, but it is not the preferred way

        // Allocate memory for 12 integers
        daysInMonth = new int[12];

        // As primitive variables, this can be done in one line
//        int[] daysInMonth = new int[12];

        // Initialize the elements
        daysInMonth[0] = 31;
        daysInMonth[1] = 28;
        daysInMonth[2] = 31;
        daysInMonth[3] = 30;
        daysInMonth[4] = 31;
        daysInMonth[5] = 30;
        daysInMonth[6] = 31;
        daysInMonth[7] = 31;
        daysInMonth[8] = 30;
        daysInMonth[9] = 31;
        daysInMonth[10] = 30;
        daysInMonth[11] = 31;

        System.out.println("February has " + daysInMonth[1] + " days.");
        System.out.println("September has " + daysInMonth[8] + " days.");

        // If you try to access an element outside the range of the array, an exception will be thrown
//        System.out.println(daysInMonth[15]);

        // You can see the length of the array by using the dot (.) operator
        System.out.println("The length of the array is " + daysInMonth.length);

        // You can use the shortcut syntax to initialize an array
        char[] myName = {'F', 'i', 'l', 'i', 'p'};
        System.out.println("The first letter of my name is " + myName[0]);

        // If you first declare and then initialize the array with shortcut syntax
//        char[] myName;
//        myName = new char[] {'F', 'i', 'l', 'i', 'p'};

        // Java initializes the items of an array with default values based on the type of the array
        // For references (anything that holds an object) that is null
        // For int/short/byte/long that is a 0
        // For float/double that is a 0.0
        // For booleans that is a false
        // For char that is the null character '\u0000' (whose decimal equivalent is 0)
        int[] numbers = new int[4];
        System.out.println(numbers[2]);
    }

}
