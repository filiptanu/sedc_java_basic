package mk.sedc.javabasic;

public class FlowControl {

    public static void main(String[] args) throws InterruptedException {
        // if statement
        boolean isJavaAwesome = true;

        if (isJavaAwesome)
            System.out.println("Java really is awesome.");

        isJavaAwesome = false;

        if (isJavaAwesome)
            System.out.println("This will not execute.");

        int hour = 17;

        if (hour <= 19) {
            System.out.println("You can still buy beer.");
            System.out.println("You must use parentheses if you want more than one statement to be executed conditionally.");
        }


        // if-else statement
        int age = 19;

        if (age >= 18)
            System.out.println("You are an adult.");
        else {
            System.out.println("You are a minor.");
            System.out.println("Remember the use of parentheses.");
        }


        // multiple if-else statements
        int num = 0;

        if (num > 0) {
            System.out.println("Number is positive.");
        } else if (num < 0) {
            System.out.println("Number is negative.");
        } else {
            System.out.println("Number is zero.");
        }


        // switch statement
        int month = 9;

        switch (month) {
            case 1:
                System.out.println("January");
                break;
            case 2:
                System.out.println("February");
                break;
            case 3:
                System.out.println("March");
                break;
            case 4:
                System.out.println("April");
                break;
            case 5:
                System.out.println("May");
                break;
            case 6:
                System.out.println("June");
                break;
            case 7:
                System.out.println("July");
                break;
            case 8:
                System.out.println("August");
                break;
            case 9:
                System.out.println("September");
                break;  // See what happens if you remove a break
            case 10:
                System.out.println("October");
                break;
            case 11:
                System.out.println("November");
                break;
            case 12:
                System.out.println("December");
                break;
        }

        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                System.out.println("This month has 31 days.");
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                System.out.println("This month has 30 days.");
                break;
            case 2:
                System.out.println("This month has 28 days.");
                break;
        }

        // Since Java 7, String can be used in a switch statement.


        // while statement
        int number = 1;

        while (number <= 10) {
            System.out.println(number);
            number++;
        }

        char letter = 'A';

        while (letter <= 'Z')
            System.out.println(letter++);

        // do-while statement
        int count = 0;

        do {
            // This will execute at least once
            System.out.println("while vs do-while");
            // Even if the condition is not met
        } while (count > 0);

        // for statement
        int[] numbers = {6, 3, 1, 7, 5, 8, 2};

        for (int i = 0; i < numbers.length; i++)
            System.out.println(numbers[i]);

        int countdown = 10;

        for(int i = countdown; i > 0; i--) {
            System.out.println(i);
            Thread.sleep(1000L); // This will pause the execution of the program for 1 second
        }


        // break statement
        int[] numbers2 = {6, 37, 1, 15, 31, 7, 5, 8, 2, 15, 87};
        int count2 = 0;
        while (count2 < numbers2.length) { // This will print the first 5 elements of the array numbers2
            if (count2 == 5)
                break;
            System.out.println(numbers2[count2++]);
        }

        // The same thing using a for loop
        for (int i = 0; i < numbers2.length; i++) {
            if (i == 5)
                break;
            System.out.println(numbers2[i]);
        }

        // continue statement
        int count3 = 1;
        while (count3 <= 10) {
            if (count3 == 4) {
                count3++;
                continue;
            }
            System.out.println(count3++);
        }

        // The same thing using a for loop
        for (int i = 1; i <= 10; i++) {
            if (i == 4)
                continue;
            System.out.println(i);
        }


        // Ternary operator
        int myAge = 25;
        boolean adult = myAge >= 18 ? true : false;

        System.out.println(adult);
    }

}