package mk.sedc.javabasic;

import java.util.ArrayList;
import java.util.List;

public class Boxing {

    public static void main(String[] args) {
        Integer i = 5;  // Autobox int to an Integer object
        int sum = 2 + i;  // Unbox an Integer object to an int

        List<Double> numbers = new ArrayList<Double>();
        numbers.add(3.4); // Autoboxing
        double number = numbers.get(0);  // Unboxing
    }

}