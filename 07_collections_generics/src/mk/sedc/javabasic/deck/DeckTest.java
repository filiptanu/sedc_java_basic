package mk.sedc.javabasic.deck;

public class DeckTest {

    public static void main(String[] args) {
        Deck deck = new Deck();
        System.out.println(deck);

        System.out.println("Shuffling...");
        deck.shuffle();
        System.out.println(deck);

        System.out.println("Dealing a card...");
        System.out.println(deck.dealCard());
        System.out.println("Cards left: " + deck.cardsLeft());
    }

}
