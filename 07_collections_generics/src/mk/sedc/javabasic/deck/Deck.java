package mk.sedc.javabasic.deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {

    private List<Card> deck;
    private int cardsUsed;

    public Deck() {
        deck = new ArrayList<Card>();

        for (int suit = 0; suit < 4; suit++) {
            for (int value = 1; value < 14; value++) {
                Card card = new Card(suit, value);
                deck.add(card);
            }
        }

        cardsUsed = 0;
    }

    public void shuffle() {
        Collections.shuffle(deck);
    }

    public Card dealCard() {
        if (!(cardsLeft() > 0))
            throw new IllegalStateException("No cards are left in the deck");

        cardsUsed++;
        return deck.get(cardsUsed - 1);
    }

    public int cardsLeft() {
        return deck.size() - cardsUsed;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Card card : deck) {
            sb.append(card);
            sb.append("\n");
        }

        return sb.toString();
    }
    
}
