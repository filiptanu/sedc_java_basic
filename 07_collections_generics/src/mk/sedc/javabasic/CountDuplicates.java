package mk.sedc.javabasic;

import java.util.HashMap;
import java.util.Map;

public class CountDuplicates {

    public static void main(String[] args) {
        int[] numbers = {4, 5, 1, 4, 2, 3, 5, 1, 8, 7, 9, 6, 5, 7, 8, 9, 2, 5};

        // key - the element, value - how many times that element appears in the array
        Map<Integer, Integer> duplicates = new HashMap<Integer, Integer>();

        for (int i = 0; i < numbers.length; i++) {
            int key = numbers[i];
            if (duplicates.containsKey(key)) {
                int count = duplicates.get(key);
                duplicates.put(key, count + 1);
            } else
                duplicates.put(key, 1);
        }

        // Check how many duplicates are in the array for a specific element
        System.out.println("4: " + duplicates.get(4));
        System.out.println("5: " + duplicates.get(4));
        System.out.println("6: " + duplicates.get(6));

        // If the key is not present in the map, null is returned
        System.out.println(duplicates.get(100));


    }

}
