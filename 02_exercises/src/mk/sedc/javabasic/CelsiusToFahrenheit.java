package mk.sedc.javabasic;

public class CelsiusToFahrenheit {

    public static void main(String[] args) {
        double celsius = 10.57;

        double fahrenheit = 9 * (celsius / 5.0) + 32;

        System.out.format("Degrees in Celsius: %.2f%n", celsius); // %.2f means "print a floating point number with 2 decimal places precision"
                                                                  // %n means "print a new line"
        System.out.format("Degrees in Fahrenheit: %.2f%n", fahrenheit);

        // You can see how System.out.format() works on the following url:
        // https://docs.oracle.com/javase/tutorial/java/data/numberformat.html
    }

}