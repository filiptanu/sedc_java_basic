package mk.sedc.javabasic;

public class SimpleCalculator {

    public static void main(String[] args) {
        int a = 34;
        int b = 10;

        int sum = a + b;
        int diff = a - b;
        int prod = a * b;
        int quot = a / b;

        System.out.println("a + b = " + sum);
        System.out.println("a - b = " + diff);
        System.out.println("a * b = " + prod);
        System.out.println("a / b = " + quot);
    }

}