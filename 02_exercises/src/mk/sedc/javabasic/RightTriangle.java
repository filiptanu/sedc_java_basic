package mk.sedc.javabasic;

public class RightTriangle {

    public static void main(String[] args) {
        int a = 3;
        int b = 4;
        int c = 5;

        boolean isRight = a * a + b * b == c * c;

        System.out.println("The sides of the triangle are:");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
        System.out.println("Is the triangle right - " + isRight);
    }

}