package mk.sedc.javabasic;

public class UppercaseToLowercase {

    public static void main(String[] args) {
        char uppercase = 'A';

        char lowercase = (char) (uppercase + 32);

        System.out.println("The lowercase character is " + lowercase);
    }

}