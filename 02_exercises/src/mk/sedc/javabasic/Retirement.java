package mk.sedc.javabasic;

public class Retirement {

    public static void main(String[] args) {
        int age = 72;
        char sex = 'M';
        int yearsWorking = 43;

        boolean canRetire = yearsWorking >= 15 && ((sex == 'M' && age >= 64) || (sex == 'F' && age >= 62));

        System.out.println("Can this person retire - " + canRetire);
    }

}