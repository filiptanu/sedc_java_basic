package mk.sedc.javabasic;

public class Rectangle {

    public static void main(String[] args) {
        int a = 7;
        int b = 4;

        int area = a * b;
        int perimeter = 2 * a + 2 * b;

        System.out.println("The sides of the rectangle are:");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("The area of the rectangle is " + area);
        System.out.println("The perimeter of the rectangle is " + perimeter);
    }

}