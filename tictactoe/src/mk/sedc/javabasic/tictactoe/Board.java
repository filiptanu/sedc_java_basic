package mk.sedc.javabasic.tictactoe;

public class Board {

    private char[] board;
    private String errorMessage;
    private char currentPlayer;

    public Board(char firstPlayer) {
        board = new char[9];

        for (int i = 0; i < board.length; i++) {
            board[i] = (char) ('0' + i);
        }

        errorMessage = "";

        currentPlayer = firstPlayer;
    }

    public void displayBoard() {
        System.out.format(" %c | %c | %c \n", board[0], board[1], board[2]);
        System.out.println("-----------");
        System.out.format(" %c | %c | %c \n", board[3], board[4], board[5]);
        System.out.println("-----------");
        System.out.format(" %c | %c | %c \n", board[6], board[7], board[8]);
        System.out.println(errorMessage);

        if (isCurrentPlayerWinner())
            System.out.println("Player " + currentPlayer + " wins!");
        else if (isTie())
            System.out.println("It's a tie.");
        else
            System.out.println("Player " + currentPlayer + ": ");

        errorMessage = "";
    }

    public void play(int field) {
        if (isFieldFree(field)) {
            board[field] = currentPlayer;

            if (!isCurrentPlayerWinner())
                swapPlayer();
        } else {
            errorMessage = "Field " + field + " is occupied.";
        }
    }

    private boolean isFieldFree(int field) {
        return board[field] != 'x' && board[field] != 'o';
    }

    private boolean isCurrentPlayerWinner() {
        if (checkLine(currentPlayer, 0, 1, 2) ||
            checkLine(currentPlayer, 3, 4, 5) ||
            checkLine(currentPlayer, 6, 7, 8) ||

            checkLine(currentPlayer, 0, 3, 6) ||
            checkLine(currentPlayer, 1, 4, 7) ||
            checkLine(currentPlayer, 2, 5, 8) ||

            checkLine(currentPlayer, 0, 4, 8) ||
            checkLine(currentPlayer, 2, 4, 6))
            return true;

        return false;
    }

    private void swapPlayer() {
        if (currentPlayer == 'x')
            currentPlayer = 'o';
        else
            currentPlayer = 'x';
    }

    public boolean isGameOver() {
        if (isCurrentPlayerWinner() || isTie())
            return true;

        return false;
    }

    private boolean isTie() {
        for (int i = 0; i < board.length; i++) {
            if (isFieldFree(i))
                return false;
        }

        return true;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    private boolean checkLine(char symbol, int field1, int field2, int field3) {
        if (board[field1] == symbol && board[field2] == symbol && board[field3] == symbol)
            return true;

        return false;
    }

}
