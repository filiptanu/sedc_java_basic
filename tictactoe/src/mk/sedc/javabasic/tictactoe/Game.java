package mk.sedc.javabasic.tictactoe;

import java.util.Scanner;

public class Game {

    public static void main(String[] args) {
        Board board = new Board('x');
        Scanner input = new Scanner(System.in);

        do {
            board.displayBoard();

            int field = input.nextInt();

            if (field >= 0 && field <= 8)
                board.play(field);
            else
                board.setErrorMessage("Enter numbers 0-8 to play...");
        } while (!board.isGameOver());

        board.displayBoard();
    }

}
