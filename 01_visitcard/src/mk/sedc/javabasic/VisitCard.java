package mk.sedc.javabasic;

/* Display your personal information on the screen */
public class VisitCard {

    public static void main(String[] args) {
        // Use System.out.println() to print stuff on the screen
        System.out.println("Filip Tanurovski");
        System.out.println("Programmer");
        System.out.println("Email: filiptanu@gmail.com");
    }

}