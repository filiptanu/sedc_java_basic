package mk.sedc.javabasic;

/* Our first Java program */
public class HelloWorld {

    // This is where the program starts executing
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

}