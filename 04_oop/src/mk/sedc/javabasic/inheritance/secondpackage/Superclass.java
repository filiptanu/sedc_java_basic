package mk.sedc.javabasic.inheritance.secondpackage;

// Example #15
public class Superclass {

    int superclassVariable;

    public Superclass(int superclassVariable) {
        this.superclassVariable = superclassVariable;
    }

    void printMethod() {
        System.out.println("Printed in superclass...");
    }

}
