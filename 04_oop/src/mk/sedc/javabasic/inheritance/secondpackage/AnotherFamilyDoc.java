package mk.sedc.javabasic.inheritance.secondpackage;

import mk.sedc.javabasic.inheritance.firstpackage.Doctor;

// This class although extends Doctor is in another package
// Because of that, it cannot access variables/methods with default access modifier
public class AnotherFamilyDoc extends Doctor {

    boolean makesHouseCalls;

    void giveAdvice() {
        System.out.println("Giving advice...");

        // Example #11
        worksAtHospital = false;

        // This will not work since name is with the default access modifier
//        name = "Fred";
    }

}
