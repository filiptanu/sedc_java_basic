package mk.sedc.javabasic.inheritance.secondpackage;

// Example #15
public class Subclass extends Superclass {

    int subclassVariable;

    public Subclass(int superclassVariable, int subclassVariable) {
        super(superclassVariable);
        this.subclassVariable = subclassVariable;
    }

    // This won't work, you must call a constructor from the superclass
    // If there is no constructor defined, the default constructor will be called automatically
//    public Subclass() {
//
//    }

    void printMethod() {
        super.printMethod();
        System.out.println("Printed in subclass...");
    }

}
