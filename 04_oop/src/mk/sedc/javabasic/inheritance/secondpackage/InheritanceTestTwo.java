package mk.sedc.javabasic.inheritance.secondpackage;

public class InheritanceTestTwo {

    public static void main(String[] args) {
        // Example #11
        AnotherFamilyDoc doc = new AnotherFamilyDoc();
        // private members cannot be accessed by anyone but the class itself
//        doc.id = 1;
        // protected members can only be accessed by the class and its children
//        doc.worksAtHospital = false;
        // package members can only be accessed in the same package
//        doc.name = "Michael";
        // public members can be accessed from anywhere
        doc.reputation = 10;

        // Example #15
        Subclass sub = new Subclass(1, 2);
        sub.printMethod();
    }



}
