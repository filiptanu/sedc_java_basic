package mk.sedc.javabasic.inheritance;

// Example #16
public final class CannotBeeExtended {

    void printMessage() {
        System.out.println("You cannot extend me, I am final.");
    }

}