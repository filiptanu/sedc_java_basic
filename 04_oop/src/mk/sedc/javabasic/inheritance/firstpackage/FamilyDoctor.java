package mk.sedc.javabasic.inheritance.firstpackage;

public class FamilyDoctor extends Doctor {

    boolean makesHouseCalls;

    void treatPatient() {
        System.out.println("Treating patient at home...");
    }

    void giveAdvice() {
        System.out.println("Giving advice...");

        // Example #12
        worksAtHospital = false;

        name = "Fred";
    }

}
