package mk.sedc.javabasic.inheritance.firstpackage;

public class InheritanceTestOne {

    public static void main(String[] args) {
        // Example #11
        FamilyDoctor familyDoctor = new FamilyDoctor();

        // Family doctor can treat a patient - something general that every doctor can do
        familyDoctor.treatPatient();
        // Family doctor can give advice - something specific that a family doctor does
        familyDoctor.giveAdvice();

        Surgeon surgeon = new Surgeon();

        surgeon.treatPatient();
        surgeon.makeIncisioin();

        // Example #12
        FamilyDoctor doc = new FamilyDoctor();
        // private members cannot be accessed by anyone but the class itself
//        doc.id = 1;
        // protected members can be accessed by the class and its children and other classes in the same package
        doc.worksAtHospital = false;
        // default members can be accessed by the class and other classes in the same package
        doc.name = "Michael";
        doc.reputation = 10;

        // You can use the parent class to reference a child object
        Doctor doc1 = new FamilyDoctor();
        Doctor doc2 = new Surgeon();

        // Example #14
        doc1.treatPatient();
        doc2.treatPatient();



        // Example #18
        // Upcasting
        Surgeon doc3 = new Surgeon();
        Doctor doc4 = doc3; // The compiler will now treat the Surgeon as a doctor
                            // but the object is still Surgeon even after the cast
        doc4.treatPatient();
        // Downcasting
        ((Surgeon) doc4).makeIncisioin();   // In order to use the surgeon methods
                                            // we have to typecast (downcast) explicitly
        // This will throw an exception
//        FamilyDoctor doc5 = (FamilyDoctor) doc4;
        // This will throw an exception as well
        // The reason is that doc5's runtime type is Doctor, not a Surgeon
//        Doctor doc5 = new Doctor();
//        Surgeon doc6 = (Surgeon) doc5;
    }

}
