package mk.sedc.javabasic.inheritance.firstpackage;

public class Surgeon extends Doctor {

    // Example #13
    void treatPatient() {
        System.out.println("Performing a surgery...");
    }

    void makeIncisioin() {
        System.out.println("Making incision...");
    }

}
