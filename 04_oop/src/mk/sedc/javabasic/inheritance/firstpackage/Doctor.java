package mk.sedc.javabasic.inheritance.firstpackage;

// Example #11
public class Doctor {

    // Example #12
    private int id;
    protected boolean worksAtHospital;
    String name;
    public int reputation;

    // The behavior for access modifiers is the same for methods

    void treatPatient() {
        System.out.println("Performing a checkup...");
    }

}
