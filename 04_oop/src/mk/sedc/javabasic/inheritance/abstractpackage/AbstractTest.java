package mk.sedc.javabasic.inheritance.abstractpackage;

// Example #17
public class AbstractTest {

    public static void main(String[] args) {
        SQLQuery sql = new SQLQuery();
        sql.processSQL("SELECT * FROM USER");
    }

}
