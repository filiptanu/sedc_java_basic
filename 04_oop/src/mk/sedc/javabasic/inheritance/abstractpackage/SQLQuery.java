package mk.sedc.javabasic.inheritance.abstractpackage;

// Example #17
public class SQLQuery extends AbstractSQLQuery {

    @Override
    protected void queryDatabase(String sql) {
        System.out.println("Querying the database: " + sql);
        System.out.println("Retrieving results...");
    }

}
