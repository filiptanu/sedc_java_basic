package mk.sedc.javabasic.inheritance.abstractpackage;

// Example #17
public abstract class AbstractSQLQuery {

    private void establishConnection() {
        System.out.println("Connecting to database...");
    }

    private void closeConnection() {
        System.out.println("Closing the connection...");
    }

    protected abstract void queryDatabase(String sql);

    public void processSQL(String sql) {
        establishConnection();
        queryDatabase(sql);
        closeConnection();
    }

}
