package mk.sedc.javabasic;

public class ObjectOrientedProgramming {

    public static void main(String[] args) {
        // Usage of the String class
        String myName = "Filip";
        System.out.println(myName);
        System.out.println(myName.length());
        String myFullName = "Filip" + " " + "Tanurovski";
        System.out.println(myFullName);
        String partOfMyName = myName.substring(0, 3);
        System.out.println(partOfMyName);
        String myLastName = "Tanurovski";
        String myFullName2 = myName.concat(myLastName);
        System.out.println(myFullName2);

        // Example #2
//        Dog myDog = new Dog();

//        Dog myDog;
//        myDog = new Dog();

//        myDog.breed = "rottweiler";
//        myDog.size = 70;
//        myDog.bark();

        // Example #3
//        myDog = null;
//        myDog.bark(); // This will throw an Exception since myDog does not reference anything

//        Dog yourDog = new Dog();

//        yourDog.breed = "chihuahua";
//        yourDog.size = 10;
//        yourDog.barkMultipleTimes(3);

        Calculator calc = new Calculator();

        System.out.println(calc.add(4, 5));
        System.out.println(calc.subtract(3, 1));
        System.out.println(calc.multiply(2, 7));
        System.out.println(calc.divide(9, 5));

        int someParam = 5;
        calc.modifyParameter(someParam);
        System.out.println(someParam);

//        Dog someDog = new Dog();
//        System.out.println(someDog.size);
//        System.out.println(someDog.name);

        Rectangle rect = new Rectangle(7, 3);
//        rect.a = 7;
//        rect.b = 3;
        System.out.println(rect.calculatePerimeter());
        System.out.println(rect.calculateArea());

        Dog myDog = new Dog();
        myDog.setSize(70);
        myDog.setBreed("Rottweiler");
        myDog.setName("Fred");

        // Example #9
        Bicycle bicycle1 = new Bicycle(3, 4);
        System.out.println(bicycle1.getId());
        Bicycle bicycle2 = new Bicycle(4, 5);
        System.out.println(bicycle2.getId());
        System.out.println(Bicycle.getNumOfBicycles());

        // Example #10
        Dog[] dogs = new Dog[3];
        dogs[0] = new Dog();
        dogs[1] = new Dog();
    }

}