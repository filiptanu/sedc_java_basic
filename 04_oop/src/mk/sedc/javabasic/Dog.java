package mk.sedc.javabasic;

public class Dog {

    // Instance variables - they represent the state of an object
    private int size;   // Example #6
    private String breed;
    private String name;

    // Example #7
    public Dog() {

    }

    public Dog(int s, String n) {
        size = s;
        name = n;
    }

    // Methods - they represent the behavior of an object
    void bark() {
        // Example #4
        if (size > 60) {
            // Example #1
            System.out.println("Wooof! Wooof!");
        } else if (size > 14) {
            System.out.println("Ruff! Ruff!");
        } else {
            System.out.println("Yip! Yip!");
        }
    }

    // Example #5
    void bark(int times) {
        while (times > 0) {
            bark();

            times--;
        }
    }

    // Example #6
    public void setSize(int s) {
        // By forcing  everybody to use a setter method
        // we can protect the dog object from unacceptable size changes
        if (s > 5)
            size = s;
    }
    public int getSize() {
        return size;
    }
    public void setBreed(String b) {
        breed = b;
    }
    public String getBreed() {
        return breed;
    }
    public void setName(String n) {
        name = n;
    }
    public String getName() {
        return name;
    }

    // Example #19
    public boolean equals(Object obj) {
        Dog other = (Dog) obj;

        return breed.equals(other.getBreed());

        // Equality by other property
//        Dog other = (Dog) obj;
//        return size == other.getSize();

        // Equality by two properties
//        return (size == other.getSize() && breed.equals(other.getBreed()));
    }
}
