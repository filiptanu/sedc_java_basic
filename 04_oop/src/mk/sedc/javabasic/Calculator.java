package mk.sedc.javabasic;

public class Calculator {

    int add(int a, int b) {
        int result = a + b;

        return result;
    }

    // Example #5
    double add(double a, double b) {
        return a + b;
    }

    // This will not work
//    double add(int a, int b) {
//        return (double) a + b;
//    }

    int add(int a, int b, int c) {
        return a + b + c;
    }

    int subtract(int a, int b) {
        int result = a - b;

        return result;
    }

    int multiply(int a, int b) {
        int result = a * b;

        return result;
    }

    int divide(int a, int b) {
        int result = a / b;

        return result;
    }

    void modifyParameter(int a) {
        a += 10;
    }

}
