package mk.sedc.javabasic;

// Example #8
public class Rectangle {

    private int x, y, width, height;

    public Rectangle() {
        this(0, 0, 1, 1);
    }

    public Rectangle(int width, int height) {
        this(0, 0, width, height);
    }

    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int calculatePerimeter() {
        int perimeter = 2 * width + 2 * height;

        return perimeter;
    }

    public int calculateArea() {
        int area = width * height;

        return area;
    }

}
