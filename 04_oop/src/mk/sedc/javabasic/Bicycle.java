package mk.sedc.javabasic;

// Example #9
public class Bicycle {

//    private static int numOfBicycles = 0;
    private static int numOfBicycles;

    {
        numOfBicycles = 0;
    }


    private int id;
    private int speed;
    private int gear;

    public Bicycle(int speed, int gear) {
        id = ++numOfBicycles;

        this.speed = speed;
        this.gear = gear;
    }

    public int getId() {
        return id;
    }

    public static int getNumOfBicycles() {
        return numOfBicycles;
    }

}
