package com.seavus.sedc.crawler;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Crawler {

    private Set<String> toCrawl;
    private Set<String> crawled;

    public Crawler(String seed) {
        toCrawl = new HashSet<String>();
        crawled = new HashSet<String>();

        toCrawl.add(seed);
    }

    public void crawl() throws IOException {
        while (!toCrawl.isEmpty()) {
            String urlToCrawl = toCrawl.iterator().next();
            toCrawl.remove(urlToCrawl);

            System.out.println("Crawling: " + urlToCrawl);

            Connection connection = Jsoup.connect(urlToCrawl);
            Document html = connection.get();
            Elements links = html.select("a[href]");

            for (Element link : links) {
                String urlToAdd = link.absUrl("href");

                if (!"".equals(urlToAdd) && !crawled.contains(urlToAdd))
                    toCrawl.add(urlToAdd);
            }

            // Here should be a method to parse the current url's content
            parseUrlContent(urlToCrawl, html);

            crawled.add(urlToCrawl);
        }
    }

    private void parseUrlContent(String url, Document html) {
        // Get content from the html that is relevant to you
        Element title = html.select("title").first();

        System.out.println("The title of the page is: " + (title != null ? title.html() : "NO TITLE"));
    }

}
