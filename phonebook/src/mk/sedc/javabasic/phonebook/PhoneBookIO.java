package mk.sedc.javabasic.phonebook;

import java.io.*;

public class PhoneBookIO {

    public static PhoneBook readPhoneBookFromFile(String fileName) {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String currentLine;

            PhoneBook phoneBook = new PhoneBook();

            while ((currentLine = br.readLine()) != null) {
                phoneBook.addEntry(parseLine(currentLine));
            }

            return phoneBook;
        } catch (IOException e) {
            return new PhoneBook();
        }
    }

    private static Entry parseLine(String line) {
        // Some logic should be added here to check if the format of the file is correct
        // For simplicity, such check is not performed
        String[] parts = line.split(",");

        Entry entry = new Entry(parts[0]);

        for (int i = 1; i < parts.length; i++) {
            entry.addNumber(parts[i]);
        }

        return entry;
    }

    public static void writePhoneBookToFile(PhoneBook phoneBook, String fileName) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            for (Entry entry : phoneBook.getPhoneBook()) {
                bw.write(entry.toString());
                bw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
