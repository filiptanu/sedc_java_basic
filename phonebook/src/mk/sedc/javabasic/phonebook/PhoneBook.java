package mk.sedc.javabasic.phonebook;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that represents a basic phone book.
 * A client can add and remove entries from a phone book.
 * For simplicity, no other operations are allowed
 */
public class PhoneBook {

    private List<Entry> phoneBook;

    public PhoneBook() {
        phoneBook = new ArrayList<Entry>();
    }

    public void addEntry(Entry entry) {
        phoneBook.add(entry);
    }

    public void removeEntry(String name) {
        Entry entry = new Entry(name);

        phoneBook.remove(entry);
    }

    public void displayPhoneBook() {
        for(Entry e : phoneBook)
            System.out.println(e);
    }

    public List<Entry> getPhoneBook() {
        return phoneBook;
    }

}
