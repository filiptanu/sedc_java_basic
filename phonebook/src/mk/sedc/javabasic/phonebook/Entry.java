package mk.sedc.javabasic.phonebook;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that represents an entry in a phone book.
 * A client can only add a number to an entry.
 * For simplicity, no other operations are allowed.
 */
public class Entry {

    private String name;
    private List<String> numbers;

    public Entry(String name) {
        this.name = name;

        numbers = new ArrayList<String>();
    }

    public void addNumber(String number) {
        numbers.add(number);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(name);

        for(String number : numbers) {
            sb.append(",");
            sb.append(number);
        }

        return sb.toString();
    }

    // This is used when getting/removing an entry from the phonebook
    @Override
    public boolean equals(Object obj) {
        Entry entry = (Entry) obj;

        return name.equals(entry.name);
    }
}
