package mk.sedc.javabasic.phonebook;

public class Main {

    public static void main(String[] args) {
        // Create a new phone book
        PhoneBook phoneBook = new PhoneBook();

        phoneBook.displayPhoneBook();

        // Add few entries in it
        Entry entry = new Entry("Filip");
        entry.addNumber("+38970123456");
        entry.addNumber("+38971123456");

        phoneBook.addEntry(entry);

        entry = new Entry("Marko");
        entry.addNumber("+38970123456");
        entry.addNumber("+38971123456");

        phoneBook.addEntry(entry);

        entry = new Entry("Stefan");
        entry.addNumber("+38970123456");
        entry.addNumber("+38971123456");

        phoneBook.addEntry(entry);

        phoneBook.displayPhoneBook();

        // Save the phone book to a file
        PhoneBookIO.writePhoneBookToFile(phoneBook, "testPhoneBook.txt");

        // Read the saved phone book from a file
        phoneBook = PhoneBookIO.readPhoneBookFromFile("testPhoneBook.txt");

        phoneBook.displayPhoneBook();

        // Remove an entry from the phone book
        phoneBook.removeEntry("Marko");

        phoneBook.displayPhoneBook();

        // Save the updated phone book to a new file
        PhoneBookIO.writePhoneBookToFile(phoneBook, "testUpdatedPhoneBook.txt");
    }

}
