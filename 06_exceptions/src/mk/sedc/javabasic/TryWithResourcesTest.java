package mk.sedc.javabasic;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

// Example #4
public class TryWithResourcesTest {

    public static void main(String[] args) {
        // Before Java 7, opened resources had to be closed manually after they are used
        FileReader fr1 = null;
        try {
            File file = new File("file.txt");
            fr1 = new FileReader(file); char [] a = new char[50];
            fr1.read(a);   // reads the content to the array
            for(char c : a)
                System.out.print(c);   // prints the characters one by one
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fr1.close();
            }catch(IOException ex) {
                ex.printStackTrace();
            }
        }

        // Java 7 introduces a new syntax for try/catch called try-with-resources (automatic resource management)
        try(FileReader fr2 = new FileReader("E://file.txt")) {
            char [] a = new char[50];
            fr2.read(a);   // reads the contentto the array
            for(char c : a)
                System.out.print(c);   // prints the characters one by one
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

}
