package mk.sedc.javabasic;

// Example #2
public class UncheckedExceptionsTest {

    public static void main(String[] args) {
        int number = 50/0; // ArithmeticException

        int array[] = new int[5];
        array[10] = 50; // ArrayIndexOutOfBoundsException

        int otherArray[] = new int[-15];  // NegativeArraySizeException

        String string = null;
        System.out.println(string.length()); // NullPointerException

        String otherString = "Some other string...";
        System.out.println(otherString.charAt(123));  // StringIndexOutOfBoundsException
    }

}