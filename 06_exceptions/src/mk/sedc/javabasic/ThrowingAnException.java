package mk.sedc.javabasic;

import java.io.FileNotFoundException;

// Example #5
public class ThrowingAnException {

    public void throwAnException() throws FileNotFoundException {
        System.out.println("This method will throw an exception...");
        throw new FileNotFoundException();

        // is the "throws" part needed if a NullPointerException was thrown?
//        throw new NullPointerException();
    }

}