package mk.sedc.javabasic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class CheckedExceptionsTest {

    public static void main(String[] args) {
        System.out.println("Code before the exception...");
        // Example #1
        File file = new File("file1.txt");
        // Example #3
        try {
            FileReader fileReader = new FileReader(file);   // Must surrond this with try/catch in order
                                                            // for the program to compile
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            System.out.println("This will execute regardless whether an exception occurred or not...");
        }

        // Without proper exception handling, this line will not be executed
        System.out.println("Code after the exception...");
    }

}
