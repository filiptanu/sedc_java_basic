package mk.sedc.javabasic.calculator;

// Example #3
public class CalculatorTest {

    public static void main(String[] args) {
        Calculator c = new Calculator();

        System.out.println("Before calling the method with exception...");

        c.divide(5, 0);

        System.out.println("After calling the method with exception...");
    }

}
