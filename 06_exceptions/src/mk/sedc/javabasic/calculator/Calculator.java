package mk.sedc.javabasic.calculator;

public class Calculator {

    public int divide(int a, int b) {
        int result = 0;

        try {
          result = a / b;
        }
        // The catch blocks should be arranged from more specific to more general
        // If the order was reversed and Exception was caught first, that catch block
        // would catch all exceptions since it is a superclass for all exceptions
        catch (ArithmeticException e) {
//            e.printStackTrace();
            System.out.println("ArithmeticException occurred...");
        }
        catch (NullPointerException e) {
//            e.printStackTrace();
            System.out.println("NullPointerException occurred...");
        }
        catch (Exception e) {
//            e.printStackTrace();
            System.out.println("Exception occurred...");
        }

        // If we want to handle multiple different exceptions in the same catch block
        // we can do it in the following way
//        catch (ArithmeticException | NullPointerException e) {
//            System.out.println("Handling multiple exceptions in the same catch block...");
//        }

        return result;
    }



}
