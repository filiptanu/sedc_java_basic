package mk.sedc.javabasic.userdefinedexception;

public class BankAccount {

    private float balance;

    public float withdraw(float amount) throws InsufficientFundsException {
        if (amount > balance)
            throw new InsufficientFundsException("I've just thrown my newly defined exception");

        return balance - amount;
    }

}
