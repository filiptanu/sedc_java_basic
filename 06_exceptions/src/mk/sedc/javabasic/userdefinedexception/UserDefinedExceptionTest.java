package mk.sedc.javabasic.userdefinedexception;

// Example #6
public class UserDefinedExceptionTest {

    public static void main(String[] args) {
        BankAccount account = new BankAccount();

      try {
          account.withdraw(150.0f);
      } catch (InsufficientFundsException e) {
          e.printStackTrace();
      }
    }

}
