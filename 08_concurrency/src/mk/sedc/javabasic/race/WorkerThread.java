package mk.sedc.javabasic.race;

/**
 * Created by filip on 03.4.2017.
 */
public class WorkerThread implements Runnable {

    private SharedResource resource;

    public WorkerThread(SharedResource resource) {
        this.resource = resource;
    }

    @Override
    public void run() {
        resource.increment();
    }
}
