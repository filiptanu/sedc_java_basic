package mk.sedc.javabasic.race;

public class RaceTest {

    public static void main(String[] args) throws InterruptedException {
        SharedResource resource = new SharedResource();

        Thread t1 = new Thread(new WorkerThread(resource));
        Thread t2 = new Thread(new WorkerThread(resource));

        t1.start();
        t2.start();

        Thread.sleep(1000);

        // The count should be 2, but depending on how the threads got executed, it could be 1 as well
        System.out.println(resource.getCount());
    }

}
