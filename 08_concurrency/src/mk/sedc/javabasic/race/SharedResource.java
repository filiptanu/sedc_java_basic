package mk.sedc.javabasic.race;

public class SharedResource {

    private int count;

    public synchronized void increment() {
        System.out.println("Incrementing count...");
        count++;
    }

    public int getCount() {
        return count;
    }

}
