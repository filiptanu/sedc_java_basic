package mk.sedc.javabasic;

public class ThreadTest {

    public static void main(String[] args) {
        Thread thread1 = new Thread(new HelloRunnable());
        Thread thread2 = new HelloThread();

        thread1.start();
        thread2.start();
    }

}
