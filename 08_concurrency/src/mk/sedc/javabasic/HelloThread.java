package mk.sedc.javabasic;

public class HelloThread extends Thread {

    @Override
    public void run() {
        System.out.println("Hello from a thread (extending Thread).");
    }
}
